﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.ckbShow = new System.Windows.Forms.CheckBox();
            this.ckbHex = new System.Windows.Forms.CheckBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.clientId_txt = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.ip_txt = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtStation = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.auto_stop_btn = new System.Windows.Forms.Button();
            this.auto_start_btn = new System.Windows.Forms.Button();
            this.AtuoTime_txt = new System.Windows.Forms.TextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(643, 85);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "连接";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(643, 114);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "订阅";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(643, 190);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 24);
            this.button3.TabIndex = 2;
            this.button3.Text = "消息推送";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(3, 75);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtInfo.Size = new System.Drawing.Size(620, 315);
            this.txtInfo.TabIndex = 15;
            this.txtInfo.WordWrap = false;
            // 
            // ckbShow
            // 
            this.ckbShow.AutoSize = true;
            this.ckbShow.Checked = true;
            this.ckbShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbShow.Location = new System.Drawing.Point(63, 36);
            this.ckbShow.Name = "ckbShow";
            this.ckbShow.Size = new System.Drawing.Size(72, 16);
            this.ckbShow.TabIndex = 17;
            this.ckbShow.Text = "显示消息";
            this.ckbShow.UseVisualStyleBackColor = true;
            // 
            // ckbHex
            // 
            this.ckbHex.AutoSize = true;
            this.ckbHex.Location = new System.Drawing.Point(139, 36);
            this.ckbHex.Name = "ckbHex";
            this.ckbHex.Size = new System.Drawing.Size(66, 16);
            this.ckbHex.TabIndex = 18;
            this.ckbHex.Text = "Hex格式";
            this.ckbHex.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(39, 422);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 19;
            this.btnClear.Text = "清除所有";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // clientId_txt
            // 
            this.clientId_txt.Location = new System.Drawing.Point(467, 36);
            this.clientId_txt.Margin = new System.Windows.Forms.Padding(2);
            this.clientId_txt.Name = "clientId_txt";
            this.clientId_txt.Size = new System.Drawing.Size(156, 21);
            this.clientId_txt.TabIndex = 20;
            this.clientId_txt.Text = "123456";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(662, 310);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(56, 18);
            this.button4.TabIndex = 21;
            this.button4.Text = "声音开始";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button
            // 
            this.button.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button.Location = new System.Drawing.Point(280, 435);
            this.button.Margin = new System.Windows.Forms.Padding(2);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(176, 8);
            this.button.TabIndex = 22;
            this.button.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(662, 401);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(56, 18);
            this.button5.TabIndex = 23;
            this.button5.Text = "声音停止";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(662, 334);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(56, 18);
            this.button6.TabIndex = 24;
            this.button6.Text = "开始播放";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // ip_txt
            // 
            this.ip_txt.Location = new System.Drawing.Point(250, 35);
            this.ip_txt.Margin = new System.Windows.Forms.Padding(2);
            this.ip_txt.Name = "ip_txt";
            this.ip_txt.Size = new System.Drawing.Size(76, 21);
            this.ip_txt.TabIndex = 25;
            this.ip_txt.Text = "127.0.0.1";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(643, 154);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(56, 18);
            this.button7.TabIndex = 26;
            this.button7.Text = "取消订阅";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(654, 37);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(56, 18);
            this.button8.TabIndex = 27;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(654, 242);
            this.button9.Margin = new System.Windows.Forms.Padding(2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(56, 18);
            this.button9.TabIndex = 28;
            this.button9.Text = "多线程测试";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtStation);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.auto_stop_btn);
            this.groupBox1.Controls.Add(this.auto_start_btn);
            this.groupBox1.Controls.Add(this.AtuoTime_txt);
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(784, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(284, 191);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "电视看板测试";
            // 
            // txtStation
            // 
            this.txtStation.Location = new System.Drawing.Point(65, 102);
            this.txtStation.Margin = new System.Windows.Forms.Padding(2);
            this.txtStation.Name = "txtStation";
            this.txtStation.Size = new System.Drawing.Size(135, 21);
            this.txtStation.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 104);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 19;
            this.label3.Text = "工位";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 140);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 18;
            this.label2.Text = "VIN码";
            // 
            // auto_stop_btn
            // 
            this.auto_stop_btn.Enabled = false;
            this.auto_stop_btn.Location = new System.Drawing.Point(209, 29);
            this.auto_stop_btn.Name = "auto_stop_btn";
            this.auto_stop_btn.Size = new System.Drawing.Size(56, 23);
            this.auto_stop_btn.TabIndex = 17;
            this.auto_stop_btn.Text = "结束";
            this.auto_stop_btn.UseVisualStyleBackColor = true;
            // 
            // auto_start_btn
            // 
            this.auto_start_btn.Location = new System.Drawing.Point(143, 29);
            this.auto_start_btn.Name = "auto_start_btn";
            this.auto_start_btn.Size = new System.Drawing.Size(55, 23);
            this.auto_start_btn.TabIndex = 16;
            this.auto_start_btn.Text = "开始";
            this.auto_start_btn.UseVisualStyleBackColor = true;
            this.auto_start_btn.Click += new System.EventHandler(this.auto_start_btn_Click);
            // 
            // AtuoTime_txt
            // 
            this.AtuoTime_txt.Location = new System.Drawing.Point(99, 31);
            this.AtuoTime_txt.Name = "AtuoTime_txt";
            this.AtuoTime_txt.Size = new System.Drawing.Size(38, 21);
            this.AtuoTime_txt.TabIndex = 15;
            this.AtuoTime_txt.Text = "30";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(209, 137);
            this.button10.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(68, 19);
            this.button10.TabIndex = 11;
            this.button10.Text = "推送消息";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(65, 137);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(135, 21);
            this.textBox1.TabIndex = 12;
            this.textBox1.Text = "LS5A2DE1113111111";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "自动推送时间";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1133, 495);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.ip_txt);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.clientId_txt);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.ckbShow);
            this.Controls.Add(this.ckbHex);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.CheckBox ckbShow;
        private System.Windows.Forms.CheckBox ckbHex;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox clientId_txt;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox ip_txt;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtStation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button auto_stop_btn;
        private System.Windows.Forms.Button auto_start_btn;
        private System.Windows.Forms.TextBox AtuoTime_txt;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
    }
}


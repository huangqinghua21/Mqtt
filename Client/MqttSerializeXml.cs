﻿using System.Text;
using mqtttLib.Helper;

namespace Client
{
    /// <summary>
    /// mqtt 序列化实体
    /// </summary>
    public class MqttSerializeXml : ISerialize
    {
        public byte[] Serializer<T>(T serialObject) where T : class
        {
            return Encoding.UTF8.GetBytes(XmlSerializeUtil.Serializer(serialObject));
        }

        public T Deserialize<T>(byte[] content) where T : class
        {
            return XmlSerializeUtil.Deserialize<T>(Encoding.UTF8.GetString(content));
        }
    }
    
}

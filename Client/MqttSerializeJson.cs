﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mqtttLib.Helper;
using Newtonsoft.Json;

namespace Client
{
    public class MqttSerializeJson : ISerialize
    {
        public byte[] Serializer<T>(T serialObject) where T : class
        { 
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(serialObject));
        }

        public T Deserialize<T>(byte[] content) where T : class
        {
            string json = Encoding.UTF8.GetString(content);
            return JsonConvert.DeserializeObject<T>(json); 
        }
    }
}

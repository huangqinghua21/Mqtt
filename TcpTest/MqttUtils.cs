﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TcpTest
{
    public static class HexLog
    {


        /// <summary>
        /// 把指定的二进制值显示为适合记入日志的文本串
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetHexLog(this byte[] data)
        {
            if (data == null) return "";
            string result = "";
            int start = 0;
            while (start < data.Length)
            {
                //if (!result.Equals("")) result += "\r\n";
                int len = data.Length - start;
                //if (len > 16) len = 16;
                string strByte = BitConverter.ToString(data, start, len);
                strByte = strByte.Replace('-', ' ');
                //if (len > 8)
                //{
                //    strByte = strByte.Substring(0, 23) + "-" + strByte.Substring(24);
                //}
                //if (len < 16)
                //{
                //    strByte += new string(' ', (16 - len) * 3);
                //}
                //strByte += "  ";
                //for (int i = 0; i < len; i++)
                //{
                //    char c = Convert.ToChar(data[start + i]);
                //    if (char.IsControl(c))
                //    {
                //        c = '.';
                //    }
                //    strByte += c;
                //}
                result += strByte;
                start += len;
            }
            return result;
        }


    }
}

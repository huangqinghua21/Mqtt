﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using mqtttLib.Helper.TcpSocket;
using mqtttLib.MatchTree;

namespace TcpTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tcp 服务
        /// </summary>
        private TcpService _server;

        /// <summary>
        /// 连接开始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void server_Connected(IDataTransmit sender, NetEventArgs e)
        {
            try
            {
                sender.ReceiveData += ReceiveData;
                //接收数据
                sender.Start();

            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }

        }


        /// <summary>
        /// 接收消息 处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReceiveData(IDataTransmit sender, NetEventArgs e)
        {
            try
            {
                byte[] data = (byte[])e.EventArg;
                _log.Info("接受客户端", data.GetHexLog());

                _commun.Send(data);


            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }
        private TcpClient _commun;
        /// <summary>
        /// 断开连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void server_DisConnect(IDataTransmit sender, NetEventArgs e)
        {
            _log.Info("断开", sender.RemoteEndPoint.ToString());

        }

        private LogInfo _log;
        private void btnStart_Click(object sender, EventArgs e)
        {
            _log = new LogInfo("log4net.config", "");
            int port;
            if (!int.TryParse(txtPort.Text, out port))
            {
                MessageBox.Show(@"输入值不是合法的数字", @"错误");
                return;
            }

            _server = new TcpService(port);
            _server.Connected += server_Connected;
            _server.DisConnect += server_DisConnect;
            _server.Start();
            string _ip = "192.18.2.120";
            _commun = new TcpClient(_ip, port, 1, true);


            _commun.ReceiveEvent += _commun_ReceiveEvent;
            bool temp = _commun.TryConnect();
            if (temp)
            {
                btnStart.Enabled = false;
                btnStop.Enabled = true;
            }
            else
            {
                _server.Stop();
                MessageBox.Show(@"连接客户端失败", @"错误");
            }


        }
        public bool Send(IDataTransmit sender, byte[] message)
        {
            if (sender.Send(message))
            {
                // Log(sender.RemoteEndPoint + " 返回数据 ", message);
                return true;

            }
            return false;
        }
        private void _commun_ReceiveEvent(byte[] data)
        {

            _log.Info("接受服务端", data.GetHexLog());

            var s = _server.Session.Values;

            foreach (var item in s)
            {
                try
                {
                    item.Send(data);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            //  sender.Send(result);
        }
        /// <summary>
        /// 目录树
        /// </summary>
        //readonly TopicTree<List<string>> _tree = new TopicTree<List<string>>();
        readonly TopicTree<string> _tree = new TopicTree<string>();
        private void button1_Click(object sender, EventArgs e)
        { 

            AddTree("adv/er24");
            AddTree("adv/23");

            AddTree("adv/er24/ss");
            AddTree("adv");
            AddTree("adv/"); 

            var list = _tree.GetList("adv/er24/ss/cc");
            string ss = "";

        }

        private void AddTree(string topicStr)
        {
            Topic topic = topicStr;

            string tempStr = "";
            foreach (var item in topic.Levels)
            {
                tempStr += item + "/";
                var temp = _tree.CollectMatches(tempStr + "#");
                if (temp!=null)
                {
                    temp.Add(topicStr);
                }
                
            }
            var temp2 = _tree.CollectMatches(tempStr + "+");
            
            if (temp2 != null)
            {
                temp2.Add(topicStr);
            }
            _tree.Add(topic, topicStr);
        }
    }
}

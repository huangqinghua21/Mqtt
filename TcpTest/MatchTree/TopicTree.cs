using System.Collections.Generic;
using mqtttLib.MatchTree;

namespace TcpTest.MatchTree
{
    public class TopicTree<T>
    {


        /// <summary>
        ///根节点
        /// </summary>
        internal TopicNode<T> RootNode = new TopicNode<T>(".");



        /// <summary>
        /// 关联通配符
        /// </summary>
        /// <param name="topic">主题通配符匹配序列</param>
        /// <param name="value">关联值</param>
        public void Add(Topic topic, T value)
        {
            RootNode.AddTopicValue(topic, 0, value);
        }

        /// <summary>
        /// 查找主动
        /// </summary>
        /// <param name="topic">匹配的完整主题</param>
        /// <returns>包含匹配的所有值的列表<code>topic</code>.</returns>
        public List<T> CollectMatches(Topic topic)
        {
            List<T> matches = new List<T>();
            RootNode.CollectMatches(topic, 0, matches);
            return matches;
        }

        /// <summary>
        /// Remove values attributed to <code>topic</code> unexpanded. i.e no wildcard matching is used.
        /// </summary>
        /// <param name="topic">Topic to Match</param>
        /// <param name="value">Value to Remove</param>
        public void Remove(Topic topic, T value)
        {
            RootNode.Remove(topic, 0, value);
        }

        /// <summary>
        /// Remove all attached values equal to <code>value</code>
        /// </summary>
        /// <param name="value">Value to Remove</param>
        public void RemoveAll(T value)
        {
            RootNode.RemoveAll(value);
        }

        /// <summary>
        /// Remove all values from the Topic Tree
        /// </summary>
        public void RemoveAll()
        {
            RootNode = new TopicNode<T>(".");
        }

    }
}

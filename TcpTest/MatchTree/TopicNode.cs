using System.Collections.Generic;
using mqtttLib.MatchTree;

namespace TcpTest.MatchTree
{
    /// <summary>
    /// 表示主题层次结构树中的级别
    /// </summary>
    internal class TopicNode<T>
    {

        #region 成员变量

        private string nodevalue;

        /// <summary>
        ///  主题层次结构
        /// </summary>
        protected Dictionary<string, TopicNode<T>> Children;

        /// <summary>
        /// 节点的相关值
        /// </summary>
        protected List<T> values = new List<T>();

        #endregion 
 
        /// <summary>
        /// 层次结构中的值
        /// </summary>
        public string Nodevalue
        {
            get { return nodevalue; }
        }

        /// <summary>
        ///节点的相关值。
        /// </summary>
        public List<T> Values
        {
            get { return values; }
        }
         

        /// <summary></summary>
        /// <param name="nodevalue">主题层次结构的此级别的值</param>
        public TopicNode(string nodevalue)
        {
            this.nodevalue = nodevalue;
        }

        /// <summary></summary>
        /// <param name="nodevalue">主题层次结构的此级别的值</param>
        /// <param name="attachedvalue">附加到该节点的值</param>
        public TopicNode(string nodevalue, T attachedvalue)
        {
            this.nodevalue = nodevalue;
            values = new List<T>();
            values.Add(attachedvalue);
        }

        /// <summary>
        /// 在主题解析过程中，将主题值添加到节点给定的当前位置
        /// </summary>
        /// <param name="t"> <code>value</code></param>
        /// <param name="level">Current Topic Level being parse</param>
        /// <param name="value">Value to store against topic</param>
        internal void AddTopicValue(Topic t, uint level, T value)
        {
            if (level == t.Levels.Length)
            {
                // We should add this value to this nodes values if it isn't already.
                if (!values.Contains(value))
                {
                    values.Add(value);
                }
            }
            else
            {
                TopicNode<T> child;

                if (Children == null)
                {
                    Children = new Dictionary<string, TopicNode<T>>();
                }

                if (!Children.TryGetValue(t.Levels[level], out child))
                {
                    // subnode doesn't exist already
                    child = new TopicNode<T>(t.Levels[level]);
                    Children.Add(t.Levels[level], child);
                }

                child.AddTopicValue(t, level + 1, value);
            }
        }

        /// <summary>
        /// Recursively collect values from this node and sub-Children that match the given topic.
        /// </summary>
        /// <param name="topic">Topic to Match</param>
        /// <param name="level">Current level being matched against</param>
        /// <param name="matches">List containing matches so far.</param>
        internal void CollectMatches(Topic topic, uint level, List<T> matches)
        {
            if (level >= topic.Levels.Length)
            {
                foreach (T value in values)
                {
                    if (!matches.Contains(value))
                    {
                        matches.Add(value);
                    }
                }
            }
            else
            {

                if (nodevalue.Equals(Topic.TOPIC_ANY_MANY.ToString()))
                {
                    CollectMatches(topic, level + 1, matches);
                }

                if (Children != null)
                {
                    string levelstr = topic.Levels[level];
                    bool collectmany = topic.Levels[level].Equals(Topic.TOPIC_ANY_MANY.ToString());

                    foreach (KeyValuePair<string, TopicNode<T>> pair in Children)
                    {
                        if (pair.Value.Matches(levelstr))
                        {
                            pair.Value.CollectMatches(topic, level + 1, matches);
                            if (collectmany)
                            {
                                pair.Value.CollectMatches(topic, level, matches);
                            }
                        }
                    }
                }

            }

        }

        /// <param name="levelstring">level hierarchy to match against</param>
        /// <returns><code>true</code>iff this node successfully matches <code>levelstring</code> given wildcard charactors</returns>
        internal bool Matches(string levelstring)
        {
            return
              levelstring.Equals(nodevalue) ||
              nodevalue.Equals(Topic.TOPIC_ANY_ONE.ToString()) ||
              nodevalue.Equals(Topic.TOPIC_ANY_MANY.ToString()) ||
              levelstring.Equals(Topic.TOPIC_ANY_ONE.ToString()) ||
              levelstring.Equals(Topic.TOPIC_ANY_MANY.ToString());
        }

        /// <summary>
        /// Remove all attached values equal to <code>value</code>
        /// </summary>
        /// <param name="value">Value to remove</param>
        internal void RemoveAll(T value)
        {
            values.Remove(value);
            if (Children != null)
            {
                foreach (TopicNode<T> child in Children.Values)
                {
                    child.RemoveAll(value);
                    // TODO: Remove useless Children
                }
            }
        }

        /// <summary>
        /// Remove values attributed to <code>topic</code> unexpanded. i.e no wildcard matching is used.
        /// </summary>
        /// <param name="topic">Topic to match</param>
        /// <param name="value">Value to remove</param>
        /// <param name="level">Current level being matched against</param>
        internal void Remove(Topic topic, uint level, T value)
        {
            if (level >= topic.Levels.Length)
            {
                // Remove from this node
                values.Remove(value);
            }
            else
            {
                if (Children != null)
                {
                    foreach (KeyValuePair<string, TopicNode<T>> pair in Children)
                    {
                        if (pair.Value.nodevalue.Equals(topic.Levels[level]))
                        {
                            pair.Value.Remove(topic, level + 1, value);
                        }
                    }
                }
            }

        }

    }
}

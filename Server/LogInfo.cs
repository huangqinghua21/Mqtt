﻿using System.IO;
using System.Threading.Tasks;
using log4net;
using mqtttLib.Messages.Base;

namespace Server
{
    /// <summary>
    /// 日志
    /// </summary>
    public class LogInfo : mqtttLib.Helper.ILog
    {

        private readonly log4net.ILog _logger;
        public LogInfo(string filePath, string name)
        {
            FileInfo configFile = new FileInfo(filePath);
            log4net.Config.XmlConfigurator.Configure(configFile);
            _logger = LogManager.GetLogger(name);
        }
        public async void Debug(object message)
        {
            // _logger.Debug(message);
            await Task.Run(() =>
            {
                _logger.Debug(message);
            });
        }
        public async void Error(object message)
        {
            //   _logger.Error(message);
            await Task.Run(() =>
            {
                _logger.Error(message);
            });
        }

        public async void Info(string title, MqttMessage message)
        {
            await Task.Run(() =>
            { 
                _logger.Info(title + " " + message.ToJson());
            });
        }
        //public void Debug(object message)
        //{
        //    // _logger.Debug(message);

        //    _logger.Debug(message);

        //}
        //public void Error(object message)
        //{
        //    _logger.Error(message);

        //}

        //public void Info(string title, MqttMessage message)
        //{

        //    _logger.Info(title + " " + message.ToJson());

        //}

    }
}

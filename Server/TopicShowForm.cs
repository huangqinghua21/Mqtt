﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Server.Model;

namespace Server
{
    public partial class TopicShowForm : Form
    {

        private Dictionary<string, Topic> _models;
        public TopicShowForm(List<Topic> models)
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = models;
            DataGridViewButtonColumn btn = new DataGridViewButtonColumn
            {
                Name = "Btn",
                HeaderText = @"操作",
                Text = "",
                DisplayIndex = 8,
                DefaultCellStyle = { NullValue = "查看客户端" }
            };
            _models = models.ToDictionary(t => t.TopicName, t => t);
            //new Dictionary<string, Topic>();
            dataGridView1.Columns.Add(btn);//将创建的列添加到UserdataGridView中 
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowindex = e.RowIndex;
                if (rowindex >= 0)
                {
                    DataGridViewColumn column = dataGridView1.Columns[e.ColumnIndex];
                    if (column is DataGridViewButtonColumn)
                    {
                        string topicName = dataGridView1.Rows[rowindex].Cells["TopicName"].Value.ToString(); //代理地址
                        var c = _models[topicName].ClientS.ToList();
                        ClientShowForm client = new ClientShowForm(c, false);
                        //client.RestartEvent += clientId =>
                        //{
                        //    RestartEvent?.Invoke(clientId);
                        //};
                        client.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"错误");
            }
        }

    }
}

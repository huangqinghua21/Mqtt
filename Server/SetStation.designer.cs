﻿namespace Server
{
    partial class SetStation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetStation));
            this.Save_btn = new System.Windows.Forms.Button();
            this.ip_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.port_txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Save_btn
            // 
            this.Save_btn.Location = new System.Drawing.Point(219, 146);
            this.Save_btn.Margin = new System.Windows.Forms.Padding(2);
            this.Save_btn.Name = "Save_btn";
            this.Save_btn.Size = new System.Drawing.Size(63, 23);
            this.Save_btn.TabIndex = 2;
            this.Save_btn.Text = "保存";
            this.Save_btn.UseVisualStyleBackColor = true;
            this.Save_btn.Click += new System.EventHandler(this.Save_btn_Click);
            // 
            // ip_txt
            // 
            this.ip_txt.Location = new System.Drawing.Point(123, 68);
            this.ip_txt.Margin = new System.Windows.Forms.Padding(2);
            this.ip_txt.Name = "ip_txt";
            this.ip_txt.Size = new System.Drawing.Size(102, 21);
            this.ip_txt.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "服务器地址:";
            // 
            // port_txt
            // 
            this.port_txt.Location = new System.Drawing.Point(238, 70);
            this.port_txt.Margin = new System.Windows.Forms.Padding(2);
            this.port_txt.Name = "port_txt";
            this.port_txt.Size = new System.Drawing.Size(44, 21);
            this.port_txt.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(226, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = ":";
            // 
            // SetStation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 254);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ip_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Save_btn);
            this.Controls.Add(this.port_txt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SetStation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设置默认配置";
            this.Load += new System.EventHandler(this.SetStation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Save_btn;
        private System.Windows.Forms.TextBox ip_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox port_txt;
        private System.Windows.Forms.Label label4;
    }
}
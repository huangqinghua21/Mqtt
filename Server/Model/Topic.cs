﻿using System.Collections.Generic;

namespace Server.Model
{
    public class Topic
    {
        public string TopicName { get; set; }

        public int Count { get; set; }
        public LinkedList<Client> ClientS { get; set; }
    }
}

﻿namespace Server.Model
{
    public class Client
    {
        public string ClientId { get; set; }
        public string Address { get; set; }
    }
}

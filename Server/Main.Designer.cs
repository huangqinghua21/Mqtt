﻿namespace Server
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.ckbShow = new System.Windows.Forms.CheckBox();
            this.ckbHex = new System.Windows.Forms.CheckBox();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.ManageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Config_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClientMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TopicMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ip_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(559, 389);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "回发消息";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(576, 32);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 19;
            this.btnStop.Text = "停止监听";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(391, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 18;
            this.label1.Text = "端口：";
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(438, 32);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(51, 21);
            this.txtPort.TabIndex = 17;
            this.txtPort.Text = "1883";
            // 
            // ckbShow
            // 
            this.ckbShow.AutoSize = true;
            this.ckbShow.Checked = true;
            this.ckbShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbShow.Location = new System.Drawing.Point(38, 36);
            this.ckbShow.Name = "ckbShow";
            this.ckbShow.Size = new System.Drawing.Size(72, 16);
            this.ckbShow.TabIndex = 15;
            this.ckbShow.Text = "显示消息";
            this.ckbShow.UseVisualStyleBackColor = true;
            // 
            // ckbHex
            // 
            this.ckbHex.AutoSize = true;
            this.ckbHex.Location = new System.Drawing.Point(114, 36);
            this.ckbHex.Name = "ckbHex";
            this.ckbHex.Size = new System.Drawing.Size(66, 16);
            this.ckbHex.TabIndex = 16;
            this.ckbHex.Text = "Hex格式";
            this.ckbHex.UseVisualStyleBackColor = true;
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(38, 61);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtInfo.Size = new System.Drawing.Size(624, 322);
            this.txtInfo.TabIndex = 14;
            this.txtInfo.WordWrap = false;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(38, 391);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "清除所有";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(495, 31);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 13;
            this.btnStart.Text = "开始监听";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // Menu
            // 
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ManageMenuItem});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(701, 25);
            this.Menu.TabIndex = 21;
            this.Menu.Text = "menuStrip1";
            // 
            // ManageMenuItem
            // 
            this.ManageMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Config_MenuItem,
            this.ClientMenuItem,
            this.TopicMenuItem});
            this.ManageMenuItem.Name = "ManageMenuItem";
            this.ManageMenuItem.Size = new System.Drawing.Size(68, 21);
            this.ManageMenuItem.Text = "服务管理";
            // 
            // Config_MenuItem
            // 
            this.Config_MenuItem.Name = "Config_MenuItem";
            this.Config_MenuItem.Size = new System.Drawing.Size(152, 22);
            this.Config_MenuItem.Text = "默认设置";
            this.Config_MenuItem.Click += new System.EventHandler(this.Config_MenuItem_Click);
            // 
            // ClientMenuItem
            // 
            this.ClientMenuItem.Name = "ClientMenuItem";
            this.ClientMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ClientMenuItem.Text = "客户端列表";
            this.ClientMenuItem.Click += new System.EventHandler(this.ClientMenuItem_Click);
            // 
            // TopicMenuItem
            // 
            this.TopicMenuItem.Name = "TopicMenuItem";
            this.TopicMenuItem.Size = new System.Drawing.Size(152, 22);
            this.TopicMenuItem.Text = "主题列表";
            this.TopicMenuItem.Click += new System.EventHandler(this.TopicMenuItem_Click);
            // 
            // ip_txt
            // 
            this.ip_txt.Location = new System.Drawing.Point(284, 32);
            this.ip_txt.Margin = new System.Windows.Forms.Padding(2);
            this.ip_txt.Name = "ip_txt";
            this.ip_txt.Size = new System.Drawing.Size(102, 21);
            this.ip_txt.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(209, 36);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "服务器地址:";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 477);
            this.Controls.Add(this.ip_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.ckbShow);
            this.Controls.Add(this.ckbHex);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.Menu);
            this.MainMenuStrip = this.Menu;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "服务器";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.CheckBox ckbShow;
        private System.Windows.Forms.CheckBox ckbHex;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem ManageMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Config_MenuItem;
        private System.Windows.Forms.ToolStripMenuItem ClientMenuItem;
        private System.Windows.Forms.TextBox ip_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem TopicMenuItem;
    }
}
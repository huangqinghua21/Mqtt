﻿using System;
using System.Windows.Forms;
using Server.Helper;

namespace Server
{
    public partial class SetStation : Form
    {
        public event Action UpdateEvent;
        public SetStation()
        {
            InitializeComponent();
        }

        private void SetStation_Load(object sender, EventArgs e)
        {
            ip_txt.Text = ConfigureAppConfig.GetConnectionStringsValue("Ip");
            port_txt.Text = ConfigureAppConfig.GetConnectionStringsValue("Port");
        }

        private void Save_btn_Click(object sender, EventArgs e)
        {
            ConfigureAppConfig.ConnectionStringsSave("Ip", ip_txt.Text.Trim());
            ConfigureAppConfig.ConnectionStringsSave("Port", port_txt.Text.Trim());
            OnUpdateevent();
            Close();
        }

        protected virtual void OnUpdateevent()
        {
            UpdateEvent?.Invoke();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Server.Model;

namespace Server
{
    public partial class ClientShowForm : Form
    {

        public event Action<string> RestartEvent;
        public ClientShowForm(List<Client> models, bool showBtn = true)
        {
            InitializeComponent();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = models;
            if (showBtn)
            {


                DataGridViewButtonColumn btn = new DataGridViewButtonColumn
                {
                    Name = "Btn",
                    HeaderText = @"操作",
                    Text = "",
                    DisplayIndex = 8,
                    DefaultCellStyle = { NullValue = "重启" }
                };
                //DataGridViewLinkColumn dlink = new DataGridViewLinkColumn(); 
                //dlink.Text = "重启";//添加的这列的显示文字，即每行最后一列显示的文字。
                //dlink.Name = "linkEdit";
                //dlink.HeaderText = @"操作";//列的标题 
                //dlink.UseColumnTextForLinkValue = true;//上面设置的dlink.Text文字在列中显示
                dataGridView1.Columns.Add(btn);//将创建的列添加到UserdataGridView中 
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowindex = e.RowIndex;
                if (rowindex >= 0)
                {
                    DataGridViewColumn column = dataGridView1.Columns[e.ColumnIndex];
                    if (column is DataGridViewButtonColumn)
                    {
                        string clientId = dataGridView1.Rows[rowindex].Cells["ClientId"].Value.ToString(); //代理地址
                        RestartEvent?.Invoke(clientId);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"错误");
            }
        }

        public void ShowData(List<Client> models)
        {
            dataGridView1.DataSource = models;
        }
    }
}

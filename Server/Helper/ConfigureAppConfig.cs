﻿using System.Configuration;

namespace Server.Helper
{
    public class ConfigureAppConfig
    {
        /// <summary>  
        /// 获取ConnectionStrings配置节中的值  
        /// </summary>  
        /// <returns></returns>  
        public static string GetConnectionStringsValue(string keyName)
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[keyName];
            return settings.ConnectionString;
        }

        /// <summary>  
        /// 保存节点中ConnectionStrings的子节点配置项的值  
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="elementValue"></param>  
        public static void ConnectionStringsSave(string keyName, string elementValue)
        {
            var config =
            ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings[keyName].ConnectionString = elementValue;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionStrings");
        }

    }
}
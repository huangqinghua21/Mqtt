﻿using mqtttLib.Messages;
using mqtttLib.Messages.Base;

namespace mqtttLib
{

    /// <summary>
    /// 消息处理
    /// </summary>
    public abstract class HandleClient
    {
        /// <summary>
        /// 解析
        /// </summary>
        /// <param name="data"></param> 
        /// <returns></returns>
        public MqttMessage Handle(byte[] data)
        { 
            var message = MqttMessage.GetDataObjectMessage(data);
            switch (message.FixedHeader.MessageType)
            {
                case MessageType.CONNACK:
                    ConnectAckMethod(message as ConnAckMessage);
                    break;
                case MessageType.PUBLISH:
                    PublishMethod(message as PublishMessage);
                    break;
                case MessageType.PUBACK:
                    PublishAckMethod(message as PublishAckMessage);
                    break;
                case MessageType.SUBACK:
                    SubscribeAckMethod(message as SubscribeAckMessage);
                    break;
                case MessageType.UNSUBACK:
                    UnsubscribeAckMethod(message as UnsubscribeAckMessage);
                    break;
                case MessageType.PINGRESP:
                    PingResMethod(message as PingRespMessage);
                    break;
                case MessageType.Restart://非标准
                    RestartMethod();
                    break;
            }
            return message;
        }
       
        /// <summary>
        /// 连接消息处理
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected abstract  void ConnectAckMethod(ConnAckMessage message);

        /// <summary>
        /// 心跳返回处理
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>

        protected abstract  void PingResMethod(PingRespMessage message);

        /// <summary>
        /// 得到推送消息
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected abstract  void PublishMethod(PublishMessage message);
        /// <summary>
        /// 订阅返回
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected abstract  void SubscribeAckMethod(SubscribeAckMessage message);
        /// <summary>
        /// 取消订阅返回
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected abstract  void UnsubscribeAckMethod(UnsubscribeAckMessage message);
        /// <summary>
        /// 发布返回
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected abstract  void PublishAckMethod(PublishAckMessage message);

        /// <summary>
        /// 重启非标准 mqtt
        /// </summary>
        protected abstract void RestartMethod();
    }
}

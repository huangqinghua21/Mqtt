﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mqtttLib.Messages.Base;

namespace mqtttLib
{
    /// <summary>
    /// 等待确定消息
    /// </summary>
    public class DetermineMessage
    {
        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime SendTime { get; set; }

        public MqttMessagePacketId Message { get; set; }
    }
}

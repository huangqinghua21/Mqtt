using mqtttLib.Messages.Base;

namespace mqtttLib.Helper
{
    /// <summary>
    /// 日志接口
    /// </summary>
    public interface ILog
    {
        /// <summary>
        /// 调试
        /// </summary>
        /// <param name="message"></param>
        void Debug(object message);
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="message"></param>
        void Error(object message);

        /// <summary>
        /// 用户日志
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        void Info(string title, MqttMessage message);


    }
}
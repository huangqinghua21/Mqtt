﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mqtttLib.Helper
{
    /// <summary>
    /// 序列号接口
    /// </summary>
    public interface ISerialize
    {
        /// <summary>
        /// 序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serialObject"></param>
        /// <returns></returns>
        byte[] Serializer<T>(T serialObject) where T : class;
        /// <summary>
        /// 反序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        T Deserialize<T>(byte[] content) where T : class;
    }
}

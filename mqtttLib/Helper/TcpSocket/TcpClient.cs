using System;
using System.Threading;

namespace mqtttLib.Helper.TcpSocket
{
    /// <summary>
    /// 客户端 接受数据委托
    /// </summary>
    public delegate void ClientReceiveHandler(byte[] data);


    /// <summary>
    /// TcpClient 工具类
    /// </summary>
    public class TcpClient
    {

        /// <summary>
        /// 客户端 接受数据 事件
        /// </summary>
        public Action<byte[]> ReceiveEvent;
        //  public event ClientReceiveHandler ReceiveEvent;
        /// <summary>
        /// 服务器地址
        /// </summary>
        private readonly string _ip;
        /// <summary>
        /// 端口
        /// </summary>
        private readonly int _port;
        /// <summary>
        /// 重试次数
        /// </summary>
        private readonly int _tryTimes;
        /// <summary>
        /// 是否长连接
        /// </summary>
        private readonly bool _longConnection;
        /// <summary>
        /// 连接类
        /// </summary>
        private System.Net.Sockets.TcpClient _client;
        /// <summary>
        /// 最后一个异常
        /// </summary>
        private Exception _lastException;
        /// <summary>
        ///长连接 接受线程
        /// </summary>
        private Thread _threadReceive;
        /// <summary>
        /// 长连接接受标志
        /// </summary>
        private bool _longReceiveMark = true;


        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="ip">服务器  IP 地址</param>
        /// <param name="port">端口</param>
        /// <param name="tryTimes">重试次数</param>
        /// <param name="longConnection">是否长连接</param>
        public TcpClient(string ip, int port, int tryTimes, bool longConnection)
        {
            _ip = ip;
            _port = port;
            _tryTimes = tryTimes;
            _longConnection = longConnection;
            //_client.Available

        }

        /// <summary>
        /// 获取最后的错误信息
        /// </summary>
        public Exception LastException
        {
            // ReSharper disable once ConvertPropertyToExpressionBody
            get { return _lastException; }
        }
        /// <summary>
        /// 尝试连接
        /// </summary>
        /// <returns></returns>
        public bool TryConnect()
        {
            //for (int i = 0; i < _tryTimes; i++)
            {
                try
                {
                    _client = new System.Net.Sockets.TcpClient(_ip, _port);

                    //长连接
                    if (_longConnection)
                    {
                        //if (_threadReceive != null)
                        //{
                        //    _threadReceive.Abort();
                        //}
                        //创建线程 监听服务器 发来的消息
                        _threadReceive = new Thread(Accept)
                        {
                            IsBackground = true//设置为后台线程
                        };

                        //开启线程
                        _threadReceive.Start();
                    }


                    return true;
                }
                catch (Exception ex)
                {
                    _lastException = ex;
                }
            }
            return false;
        }
        /// <summary>
        /// 尝试关闭
        /// </summary>
        private void TryClose()
        {
            if (!_longConnection)
            {
                _client.Client?.Close();
                _client?.Close();
                _client = null;
            }
        }
        /// <summary>
        /// 尝试发送
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private bool TrySend(byte[] data)
        {
            for (int i = 0; i < _tryTimes; i++)
            {
                try
                {
                    if (data != null)
                        _client.Client.Send(data);
                    return true;
                }
                catch (Exception ex)
                {
                    TryConnect();
                    _lastException = ex;
                }
            }
            return false;
        }
        /// <summary>
        /// 尝试接收
        /// </summary>
        /// <returns></returns>
        private byte[] TryReceive()
        {
            byte[] buf = new byte[8192];
            int size = 0;
            for (int i = 0; i < _tryTimes; i++)
            {
                try
                {
                    //会产生阻塞
                    size = _client.Client.Receive(buf);
                    break;
                }
                catch (Exception ex)
                {
                    _lastException = ex;
                }
            }
            //返回数据
            if (size > 0)
            {
                byte[] data = new byte[size];
                Array.Copy(buf, data, size);
                return data;
            }
            return null;
        }

        /// <summary>
        /// 停止
        /// </summary>
        public void Stop()
        {
            _longReceiveMark = false;
            TryClose();
        }

        /// <summary>
        /// 长连接 接受消息
        /// </summary>
        private void Accept()
        {
            while (_longReceiveMark)
            {
                try
                {
                    //尝试连接中如果没有消息会产生阻塞
                    byte[] data = TryReceive();
                    if (data==null)
                    {
                        Thread.Sleep(100);
                    }
                    OnReceiveEvent(data);
                }
                catch (Exception ex)
                {
                    // ReSharper disable once PossibleIntendedRethrow
                    throw ex;
                }
            }
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="sendData">待发送的数据</param>
        /// <returns></returns>
        public bool Send(byte[] sendData)
        {
            if (_client == null)
            {
                //连接远程主机
                if (!TryConnect()) return false;
            }

            //发送数据
            bool result = TrySend(sendData);

            if (!result)
            {
                _client = null;
                CloseMonitor();
            }
            
            //关闭连接
            TryClose();
            return result;
        }

        /// <summary>
        /// 发送数据并接收数据
        /// </summary>
        /// <param name="sendData">待发送的数据</param>
        /// <returns></returns>
        public byte[] SendAndReceive(byte[] sendData)
        {
            if (_longConnection)
            {
                throw new LongConnectionException();
            }
            if (_client == null)
            {
                //连接远程主机
                if (!TryConnect()) return null;
            }

            byte[] data = null;
            //发送数据
            if (TrySend(sendData))
            {
                //接收数据
                data = TryReceive();
            }

            //关闭连接
            TryClose();
            return data;
        }
        /// <summary>
        /// 关闭长连接 监听
        /// </summary>
        public void CloseMonitor()
        {
            if (_threadReceive != null)
            {
                _threadReceive.Abort();
            }
        }
        /// <summary>
        /// 触发 接受数据事件
        /// </summary>
        /// <param name="data"></param>
        protected virtual void OnReceiveEvent(byte[] data)
        {
            ReceiveEvent?.Invoke(data);
        }
    }
    /// <summary>
    /// 长连接异常
    /// </summary>
    public class LongConnectionException : Exception
    {
        public override string Message
        {
            get
            {
                return @"长连接,请用Send发送,用ReceiveEvent接受";
            }
        }

    }
}

using System;

namespace mqtttLib.Helper.TcpSocket
{
    /// <summary>
    /// 网络通讯事件模型委托
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e">TcpClient</param>
    public delegate void NetEventHandler(IDataTransmit sender, NetEventArgs e);

    /// <summary>
    /// 网络事件参数
    /// </summary>
    public class NetEventArgs : EventArgs
    {
        private object _eventArg;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="eventArg"></param>
        public NetEventArgs(object eventArg)
        {
            _eventArg = eventArg;
        }
        /// <summary>
        /// 事件参数
        /// </summary>
        public object EventArg
        {
            get { return _eventArg; }
            set { _eventArg = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (_eventArg != null)
            {
                return _eventArg.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}

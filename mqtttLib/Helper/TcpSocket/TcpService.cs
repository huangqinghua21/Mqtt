using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

namespace mqtttLib.Helper.TcpSocket
{

    /// <summary>
    /// TCP连接服务器端,接受多客户的TCP连接
    /// </summary>
    public class TcpService<T>
         where T : class, IDataTransmit,new()
    {
        #region 事件定义

        /// <summary>
        /// 客户端连接事件
        /// </summary>
        public event NetEventHandler Connected;
        /// <summary>
        /// 客户端断开事件
        /// </summary>
        public event NetEventHandler DisConnect;
        #endregion

        #region 字段
        private readonly int _maxsockets;            //最大客户连接数
        private int _backlog;                        //最大挂起连接数
        private readonly int _port;                           //监听端口
        private readonly TcpListener _listener;               //监听类
        private readonly Dictionary<EndPoint, T> _session;    //保存连接的客户端
        private bool _active;                //是否处于活动状态
        #endregion

        #region 属性
        /// <summary>
        /// 是否处于活动状态，即自动接收客户端连接
        /// </summary>
        public bool Active
        {
            // ReSharper disable once ConvertPropertyToExpressionBody
            get { return _active; }
        }
        /// <summary>
        /// 获取监听端口号
        /// </summary>
        // ReSharper disable once ConvertToAutoProperty
        public int ListenPort
        {
            // ReSharper disable once ConvertPropertyToExpressionBody
            get { return _port; }
        }
        /// <summary>
        /// 获取当前客户连接数
        /// </summary>
        public int ConnectCount
        {
            // ReSharper disable once ConvertPropertyToExpressionBody
            get { return _session.Count; }
        }

        /// <summary>
        /// 获取与客户连接的所有Socket
        /// </summary>
        public Dictionary<EndPoint, T> Session
        {
            // ReSharper disable once ConvertPropertyToExpressionBody
            get { return _session; }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 使用指定端口、最大客户连接数、IP地址构造实例
        /// </summary>
        /// <param name="port">监听的端口号</param>
        /// <param name="maxsockets">最大客户连接量</param>
        /// <param name="ip">IP地址</param>
        public TcpService(int port, int maxsockets, string ip)
        {
            if (maxsockets < 1)
            {
                // ReSharper disable once UseNameofExpression
                throw new ArgumentOutOfRangeException("maxsockets", "最大连接数不能小于1");
            }
            _port = port;
            _maxsockets = maxsockets;
            _listener = new TcpListener(new IPEndPoint(IPAddress.Parse(ip), port));
            _session = new Dictionary<EndPoint, T>();
        }

        /// <summary>
        /// 使用指定端口构造实例
        /// </summary>
        /// <param name="port">监听的端口</param>
        public TcpService(int port)
            : this(port, 1000, "0.0.0.0")
        {
        }
        #endregion

        #region 公用方法
        /// <summary>
        /// 启动服务器程序,开始监听客户端请求
        /// </summary>
        /// <param name="backlog">挂起连接队列的最大长度。</param>
        public void Start(int backlog)
        {
            _backlog = backlog;
            _listener.Start(backlog);
            //监听客户端连接请求 
            _listener.BeginAcceptSocket(ClientConnect, _listener);
            _active = true;
        }

        /// <summary> 
        /// 启动服务器程序,开始监听客户端请求
        /// </summary> 
        public void Start()
        {
            Start(10);
        }
        /// <summary>
        /// 关闭侦听器。
        /// </summary>
        public void Stop()
        {
            _listener.Stop();
            _active = false;
        }
        /// <summary>
        /// 断开所有客户端连接
        /// </summary>
        public void DisConnectAll()
        {
            foreach (KeyValuePair<EndPoint, T> kvp in _session)
            {
                kvp.Value.DisConnected -= work_DisConnect;
                kvp.Value.Stop();
                //触发客户断开事件
                NetEventHandler handler = DisConnect;
                handler?.Invoke(kvp.Value, new NetEventArgs(new SocketException((int)SocketError.Success)));
            }
            _session.Clear();
        }
        /// <summary>
        /// 关闭侦听器并断开所有客户端连接
        /// </summary>
        public void Close()
        {
            Stop();
            DisConnectAll();
        }
        /// <summary>
        /// 客户端连接
        /// </summary>
        /// <param name="ar"></param>
        private void ClientConnect(IAsyncResult ar)
        {
            TcpListener listener = (TcpListener)ar.AsyncState;
            //接受客户的连接,得连接Socket
            try
            {
                Socket client = listener.EndAcceptSocket(ar);
                client.IOControl(IOControlCode.KeepAliveValues, Keepalive(0, 60000, 5000), null);

                //DataTransmit ==T
                T work = new T {TcpSocket = client};
                work.DisConnected += work_DisConnect;

                EndPoint socketPoint = client.RemoteEndPoint;
                if (_session.ContainsKey(socketPoint))
                {
                    _session[socketPoint] = work;
                }
                else
                {
                    _session.Add(socketPoint, work);
                }

                if (ConnectCount < _maxsockets)
                {
                    //继续监听客户端连接请求 
                    listener.BeginAcceptSocket(ClientConnect, listener);
                }
                else
                {   //达到最大连接客户数,则关闭监听.
                    listener.Stop();
                    _active = false;
                }

                //客户端连接成功事件 



                Connected?.Invoke(work, new NetEventArgs("接受客户的连接请求"));

                //每一个连接创出一个新的 线程进行处理
                //Thread clientservice = new Thread(ReceiveThread);
                //clientservice.Start(work);

                Debug.WriteLine(socketPoint + " is Connection...Num" + ConnectCount);
            }
            catch
            {
                // ignored
            }
        }
        public void ReceiveThread(object senderObject)
        {
            IDataTransmit sender = senderObject as IDataTransmit;
            Connected?.Invoke(sender, new NetEventArgs("接受客户的连接请求"));
           // Debug.WriteLine(socketPoint + " is Connection...Num" + ConnectCount);
        }

        //客户端断开连接
        private void work_DisConnect(IDataTransmit work, NetEventArgs e)
        {
            EndPoint socketPoint = work.RemoteEndPoint;
            _session.Remove(socketPoint);

            //如果已关闭侦听器,则打开,继续监听
            if (ConnectCount == _maxsockets)
            {
                _listener.Start(_backlog);
                //IAsyncResult iar =  _listener.BeginAcceptSocket(ClientConnect, _listener);
                _listener.BeginAcceptSocket(ClientConnect, _listener);
                _active = true;
            }

            //触发客户断开事件
            NetEventHandler handler = DisConnect;
            handler?.Invoke(work, e);
            Debug.WriteLine(socketPoint + " is OnDisConnected...Num" + ConnectCount);
        }
        #endregion
        
        /// <summary>
        ///  得到tcp_keepalive结构值
        /// </summary>
        /// <param name="onoff">是否启用Keep-Alive</param>
        /// <param name="keepalivetime">多长时间后开始第一次探测（单位：毫秒）</param>
        /// <param name="keepaliveinterval">探测时间间隔（单位：毫秒）</param>
        /// <returns></returns>
        public static byte[] Keepalive(int onoff, int keepalivetime, int keepaliveinterval)
        {
            byte[] inOptionValues = new byte[12];
            BitConverter.GetBytes(onoff).CopyTo(inOptionValues, 0);
            BitConverter.GetBytes(keepalivetime).CopyTo(inOptionValues, 4);
            BitConverter.GetBytes(keepaliveinterval).CopyTo(inOptionValues, 8);
            return inOptionValues;
        }
    }

    /// <summary>
    /// TCP连接服务器端,接受多客户的TCP连接
    /// </summary>
    public class TcpService : TcpService<DataTransmit>
    {
        #region 构造函数
        /// <summary>
        /// 使用指定端口、最大客户连接数、IP地址构造实例
        /// </summary>
        /// <param name="port">监听的端口号</param>
        /// <param name="maxsockets">最大客户连接量</param>
        /// <param name="ip">IP地址</param>
        public TcpService(int port, int maxsockets, string ip)
            : base(port, maxsockets, ip)
        {
        }
        /// <summary>
        /// 使用指定端口构造实例
        /// </summary>
        /// <param name="port">监听的端口</param>
        public TcpService(int port)
            : base(port, 1000, "0.0.0.0")
        {
        }
        #endregion
    }
}

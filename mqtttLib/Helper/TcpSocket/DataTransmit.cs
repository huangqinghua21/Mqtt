using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace mqtttLib.Helper.TcpSocket
{
    /// <summary>
    /// 辅助传输对象
    /// </summary>
    public class DataTransmit : IDataTransmit
    {
        #region 事件定义
        /// <summary>
        /// 连接成功事件
        /// </summary>
        public event NetEventHandler ConnectSucceed;
        /// <summary>
        /// 连接失败事件
        /// </summary>
        public event NetEventHandler ConnectFail;
        /// <summary>
        /// 断开连接事件
        /// </summary>
        public event NetEventHandler DisConnected;
        /// <summary>
        /// 接收到数据事件
        /// </summary>
        public event NetEventHandler ReceiveData;
        #endregion

        #region 字段
        private Socket _socket;                  //连接的Socket
        private EndPoint _iep;                   //网络终节点,用于标识不同的用户
        private readonly byte[] _buffer;                  //接收数据缓存
        private SocketError _errorCode;          //错误代码
        /// <summary>
        /// 缓存大小
        /// </summary>
        public const int BagSize = 8192;        //缓存大小
        #endregion

        #region 属性
        /// <summary>
        /// 获取或设置 Socket 对象
        /// </summary>
        public Socket TcpSocket
        {
            get
            {
                return _socket;
            }
            set
            {
                if (value == null)
                {
                    // ReSharper disable once NotResolvedInText
                    throw new ArgumentNullException(@"client");
                }
                _socket = value;
                _socket.ReceiveBufferSize = BagSize;
                _iep = value.RemoteEndPoint;
            }
        }
        /// <summary>
        /// 获取远程终结点
        /// </summary>
        public EndPoint RemoteEndPoint
        {
            // ReSharper disable once ConvertPropertyToExpressionBody
            get { return _iep; }
        }
        /// <summary>
        /// Socket是否已连接
        /// </summary>
        public bool Connected
        {
            get
            {
                if (_socket == null)
                {
                    return false;
                }
                else
                {
                    return _socket.Connected;
                }
            }
        }
        /// <summary>
        /// Socket错误代码
        /// </summary>
        public SocketError ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 默认构造函数
        /// </summary>
        public DataTransmit()
        {
            _errorCode = SocketError.Success;
            _buffer = new byte[BagSize];
        }
        /// <summary>
        /// 使用指定的IP地址和端口构造实例
        /// </summary>
        /// <param name="ip">IP地址</param>
        /// <param name="port">端口</param>
        public DataTransmit(string ip, int port)
            : this(new IPEndPoint(IPAddress.Parse(ip), port))
        {
        }

        /// <summary>
        /// 客户端调用此构造函数
        /// </summary>
        /// <param name="ipEndPoint">在连接的服务器端网络地址</param>
        public DataTransmit(EndPoint ipEndPoint)
            : this()
        {
            _iep = ipEndPoint;
        }

        /// <summary>
        /// 服务器端调用
        /// </summary>
        /// <param name="client">服务器监听连接得到的Socket对象</param>
        public DataTransmit(Socket client)
            : this()
        {
            TcpSocket = client;
        }
        #endregion

        /// <summary>
        /// 停止传输，断开连接
        /// </summary>
        public void Stop()
        {
            if (_socket != null)
            {
                try
                {
                    if (_socket.Connected)
                    {
                        _socket.Shutdown(SocketShutdown.Both);
                        _socket.Disconnect(false);
                        OnDisConnected(new SocketException((int)SocketError.Success));
                    }
                    _socket.Close();
                }
                catch
                {
                    // ignored
                }
                _socket = null;
            }
        }

        /// <summary>
        /// 开始接收数据
        /// </summary>
        /// <returns></returns>
        public void Start()
        {
            if (_socket != null && _socket.Connected)
            {
                receiveData();
            }
            else
            {
                // ReSharper disable once UseObjectOrCollectionInitializer
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _socket.ReceiveBufferSize = BagSize;
                _socket.BeginConnect(_iep, ConnectCallback, _socket);
            }
        }
        /// <summary>
        /// 连接回调 函数 （连接处理函数）
        /// </summary>
        /// <param name="ar"></param>
        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                _socket.EndConnect(ar);
            }
            catch (Exception err)
            {
                OnConnectFail(err);
                return;
            }
            //连接成功,开始接收数据
            OnConnectSucceed();
            receiveData();
        }
        /// <summary>
        /// 接收数据
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public void receiveData()
        {
            // 调用异步方法 BeginReceive 来告知 _socket 如何接收数据
            if (_socket != null && _socket.Connected)
            {
                //IAsyncResult iar = _socket.BeginReceive(_buffer, 0, BagSize, SocketFlags.None, out _errorCode, receiveCallback, _buffer);
                _socket.BeginReceive(_buffer, 0, BagSize, SocketFlags.None, out _errorCode, receiveCallback, _buffer);
                if ((_errorCode != SocketError.Success) && (_errorCode != SocketError.IOPending))
                {
                    OnDisConnected(new SocketException((int)_errorCode));
                }
            }
        }

        /// <summary>
        /// 接收数据回调函数
        /// </summary>
        /// <param name="ar"></param>
        // ReSharper disable once InconsistentNaming
        private void receiveCallback(IAsyncResult ar)
        {
            if (_socket == null || !_socket.Connected) return;

            //接收到的数据长度．
            int receLen;
            try
            {
                receLen = _socket.EndReceive(ar, out _errorCode);
            }
            catch (Exception err)
            {
                OnDisConnected(err);
                return;
            }
            if (_errorCode == SocketError.Success)
            {
                if (receLen > 0)
                {
                    byte[] currentBin = new byte[receLen];
                    Buffer.BlockCopy(_buffer, 0, currentBin, 0, receLen);
                    //接收事件
                    OnReceiveData(currentBin);
                    receiveData();
                }
                else
                {
                    OnDisConnected(new SocketException((int)SocketError.NotConnected));
                }
            }
            else
            {
                OnDisConnected(new SocketException((int)_errorCode));
            }
        }

        /// <summary>
        /// 发送文本
        /// </summary>
        /// <param name="text">文本内容</param>
        /// <returns></returns>
        public virtual bool Send(string text)
        {
            byte[] bin = Encoding.Default.GetBytes(text);
            return Send(bin);
        }

        /// <summary>
        /// 发送二进制数据
        /// </summary>
        /// <param name="data">二进制数据</param>
        /// <returns></returns>
        public virtual bool Send(byte[] data)
        {
            if (Connected)
            {
                _socket.BeginSend(data, 0, data.Length, SocketFlags.None, out _errorCode, SendCallBack, _socket);
                if (_errorCode == SocketError.Success)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="ar"></param>
        private void SendCallBack(IAsyncResult ar)
        {
            if (_socket == null) return;
            try
            {
                _socket.EndSend(ar, out _errorCode);
            }
            catch (Exception err)
            {
                OnDisConnected(err);
                return;
            }
            if (_errorCode != SocketError.Success)
            {
                OnDisConnected(new SocketException((int)_errorCode));
            }
        }

        #region 受保护的事件处理方法

        /// <summary>
        /// 触发连接成功事件
        /// </summary>
        protected virtual void OnConnectSucceed()
        {
            NetEventHandler hander = ConnectSucceed;
            if (hander != null)
            {
                // ReSharper disable once PossibleNullReferenceException
                ConnectSucceed(this, new NetEventArgs("成功连接到服务器"));
            }
        }

        /// <summary>
        /// 触发连接失败事件
        /// </summary>
        /// <param name="err"></param>
        protected virtual void OnConnectFail(Exception err)
        {
            NetEventHandler hander = ConnectFail;   //连接服务器失败事件
            if (hander != null)
            {
                // ReSharper disable once PossibleNullReferenceException
                ConnectFail(this, new NetEventArgs(err));
            }
        }

        /// <summary>
        /// 触发连接断开事件
        /// </summary>
        /// <param name="err"></param>
        protected virtual void OnDisConnected(Exception err)
        {
            //Stop();
            NetEventHandler hander = DisConnected;  //断开连接事件
            hander?.Invoke(this, new NetEventArgs(err));
        }

        /// <summary>
        /// 触发接收数据事件
        /// </summary>
        /// <param name="bin"></param>
        protected virtual void OnReceiveData(object bin)
        {
          //  ReceiveData(this, new NetEventArgs(bin));
            NetEventHandler hander = ReceiveData;   //接收到消息事件
            hander?.Invoke(this, new NetEventArgs(bin));
        }
        #endregion
    }
}

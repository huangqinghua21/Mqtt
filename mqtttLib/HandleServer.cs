﻿using System;
using System.Threading.Tasks;
using mqtttLib.Helper.TcpSocket;
using mqtttLib.Messages;
using mqtttLib.Messages.Base;

namespace mqtttLib
{
    /// <summary>
    /// 服务端消息处理
    /// </summary>
    public abstract class HandleServer
    {



        public MqttMessage Handle(IDataTransmit sender, byte[] data)
        {
            var message = MqttMessage.GetDataObjectMessage(data);

            //异步处理
            Task.Run(() =>
             {
                 switch (message.FixedHeader.MessageType)
                 {
                     case MessageType.CONNECT:
                         ConnectMethod(sender, message as ConnectMessage);
                         break;
                     case MessageType.CONNACK:
                         break;
                     case MessageType.PUBLISH:
                         PublishMethod(sender, message as PublishMessage);
                         break;
                     case MessageType.PUBACK:
                         break;
                     case MessageType.PUBREC:
                         break;
                     case MessageType.PUBREL:
                         break;
                     case MessageType.PUBCOMP:
                         break;
                     case MessageType.SUBSCRIBE:
                         SubscribeMethod(sender, message as SubscribeMessage);
                         break;
                     case MessageType.UNSUBSCRIBE:
                         UnsubscribeMethod(sender, message as UnsubscribeMessage);
                         break;

                     case MessageType.PINGREQ:
                         PingMethod(sender, message as PingReqMessage);
                         break;
                     case MessageType.DISCONNECT:
                         DisconnectMethod(sender, message as DisconnectMessage);
                         break;
                     default:
                         throw new ArgumentOutOfRangeException();
                 }
             });
            return message;
        }



        /// <summary>
        /// 连接消息处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public abstract void ConnectMethod(IDataTransmit sender, ConnectMessage message);
        /// <summary>
        /// 订阅
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public abstract void PublishMethod(IDataTransmit sender, PublishMessage message);
        /// <summary>
        /// 订阅
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public abstract void SubscribeMethod(IDataTransmit sender, SubscribeMessage message);

        /// <summary>
        /// 心跳处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public abstract void PingMethod(IDataTransmit sender, PingReqMessage message);
        /// <summary>
        /// 断开连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public abstract void DisconnectMethod(IDataTransmit sender, DisconnectMessage message);
        /// <summary>
        /// 取消订阅
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public abstract void UnsubscribeMethod(IDataTransmit sender, UnsubscribeMessage message);
    }
}

using System.Collections.Generic;
using System.Diagnostics;

namespace mqtttLib.MatchTree
{
    /// <summary>
    /// 主题空间的类
    /// </summary>
    public class Topic
    { 

        /// <summary>
        /// 表示层次的任何值的主题层次通配符
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public const char TOPIC_ANY_ONE = '+';

        /// <summary>
        ///主题层次通配符表示任何级别的任何值，包括0。
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public const char TOPIC_ANY_MANY = '#';

        /// <summary>
        /// seperator话题层次
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public const char TOPIC_SEPERATOR = '/'; 
        public string[] Levels { get; private set; }

        public List<string> Data { get; set; }

        /// <summary>
        /// 从主题字符串构造主题
        /// </summary>
        /// <param name="topicname">主题层次结构的字符串表示形式</param>
        public Topic(string topicname)
        {
            Levels = topicname.Split(TOPIC_SEPERATOR);
        }
 

        public static implicit operator Topic(string str)
        {
            return new Topic(str);
        }
         
    }
}

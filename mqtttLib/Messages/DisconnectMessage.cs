﻿using mqtttLib.Messages.Base;

namespace mqtttLib.Messages
{
    /// <summary>
    /// 断开消息
    /// </summary>
    public sealed class DisconnectMessage : MqttMessage
    {
        public DisconnectMessage()
            : base(MessageType.DISCONNECT)
        {
        }

        public override void GetDataObject(byte[] buffer)
        {
        }
    }
}

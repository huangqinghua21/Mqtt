﻿using mqtttLib.Messages.Base;

namespace mqtttLib.Messages
{
    /// <summary>
    /// ping
    /// </summary>
	public sealed class PingReqMessage : MqttMessage
    {
        public PingReqMessage()
            : base(MessageType.PINGREQ)
        {
        }

        public override void GetDataObject(byte[] buffer)
        { 
        }
    }

    /// <summary>
    /// ping返回
    /// </summary>
    public class PingRespMessage : MqttMessage
    {
        public PingRespMessage()
            : base(MessageType.PINGRESP)
        {
        }

        public override void GetDataObject(byte[] buffer)
        { 
        }
    }
}

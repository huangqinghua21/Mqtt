﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mqtttLib.Messages.Base;

namespace mqtttLib.Messages
{
    /// <summary>
    /// 服务器 发送命令让客户端重启 非标准 mqtt
    /// </summary>
    public class RestartMessage : MqttMessage
    {
        public RestartMessage() : base(MessageType.Restart)
        {
        }

        public override void GetDataObject(byte[] buffers)
        {
        }
    }
}

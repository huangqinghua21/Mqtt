﻿using System;

namespace mqtttLib.Messages.Base
{
    /// <summary>
    /// 服务质量等级
    /// </summary>
    [Flags]
    public enum Qos : byte
    {
        /// <summary>
        ///     QOS 0 最多一次  消息不能保证传递。没有重试
        /// </summary>
        AtMostOnce = 0,
        /// <summary>
        ///     QOS  1 至少一次 - 消息保证交付。它将至少交付一次，但可以交付，如果发生网络错误，则不止一次。
        ///     
        /// </summary>
        AtLeastOnce = 1,
        /// <summary>
        ///     QOS  2 只有一次 
        /// </summary>
        ExactlyOnce = 2,
    }
}

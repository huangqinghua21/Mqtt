﻿namespace mqtttLib.Messages.Base
{ 
    /// <summary>
    /// 具有 id的报文
    /// </summary>
    public abstract class MqttMessagePacketId : MqttMessage
    {
        private static short _packetIdentifier = 1;
        /// <summary>
        /// 得到包id
        /// </summary>
        /// <returns></returns>
        protected short GetPacketIdentifier()
        {
            lock (this)
            {
                if (32767 <= _packetIdentifier)
                    _packetIdentifier = 0;
                return _packetIdentifier++;
            }
        }
        /// <summary>
        /// 消息编号
        /// </summary>
        public short PacketIdentifier { get; set; }

        protected MqttMessagePacketId(MessageType msgType) : base(msgType)
        {
        }
    }
}

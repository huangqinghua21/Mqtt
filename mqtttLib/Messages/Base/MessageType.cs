﻿using System;

namespace mqtttLib.Messages.Base
{

    /// <summary>
    /// 消息类型
    /// </summary>
    [Flags]
    public enum MessageType : byte
    {
        //连接
        CONNECT = 1,
        /// <summary>
        /// 连接返回
        /// </summary>
        CONNACK = 2,
        /// <summary>
        /// 发布消息
        /// </summary>
        PUBLISH = 3,
        /// <summary>
        /// 发布返回
        /// </summary>
        PUBACK = 4,
        /// <summary>
        /// QoS2消息回执
        /// </summary>
        PUBREC = 5,
        /// <summary>
        /// QoS2消息释放
        /// </summary>
        PUBREL = 6,
        /// <summary>
        /// QoS2消息完成
        /// </summary>
        PUBCOMP = 7,
        /// <summary>
        /// 订阅主题
        /// </summary>
        SUBSCRIBE = 8,
        /// <summary>
        /// 订阅返回
        /// </summary>
        SUBACK = 9,
        /// <summary>
        /// 取消订阅
        /// </summary>
        UNSUBSCRIBE = 10,
        /// <summary>
        /// 取消订阅回执
        /// </summary>
        UNSUBACK = 11,
        /// <summary>
        /// PING请求
        /// </summary>
        PINGREQ = 12,
        /// <summary>
        /// PING响应
        /// </summary>
        PINGRESP = 13,
        /// <summary>
        /// 断开连接
        /// </summary>
        DISCONNECT = 14,
        /// <summary>
        /// 重启 非标准mqtt
        /// </summary>
        Restart = 15
    }

}

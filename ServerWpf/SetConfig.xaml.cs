﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ServerWpf.Helper;

namespace ServerWpf
{
    /// <summary>
    /// SetConfig.xaml 的交互逻辑
    /// </summary>
    public partial class SetConfig
    {
        public SetConfig()
        {
            InitializeComponent();
            TxtIp.Text = ConfigureAppConfig.GetConnectionStringsValue("Ip");
            TxtPort.Text = ConfigureAppConfig.GetConnectionStringsValue("Port");
        }
        public event Action UpdateEvent;
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            ConfigureAppConfig.ConnectionStringsSave("Ip", TxtIp.Text.Trim());
            ConfigureAppConfig.ConnectionStringsSave("Port", TxtPort.Text.Trim());
            OnUpdateevent();
            Close();
        }

        protected virtual void OnUpdateevent()
        {
            UpdateEvent?.Invoke();
        }
    }
}

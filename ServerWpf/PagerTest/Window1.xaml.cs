﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows;

namespace ServerWpf.PagerTest
{
    public static class Helper
    {
        /// <summary>
        /// 将List转换成DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dt = new DataTable();
            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor property = properties[i];
                dt.Columns.Add(property.Name, property.PropertyType);
            }
            object[] values = new object[properties.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }
                dt.Rows.Add(values);
            }
            return dt;
        }
    }

    /// <summary>
    /// Window1.xaml 的交互逻辑
    /// </summary>
    public partial class Window1
    {
        public Window1()
        {
            InitializeComponent();
        }
        List<Products> Prolist = new List<Products>();
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                Products p = new Products();
                p.ProID = i + 1;
                p.ProName = "Product" + i;
                p.ProCategory = ProductCategory.Deception;
                p.ProImage = @"\ImageSource\" + (i + 1) + ".png";
                p.ProLink = new Uri("http://www.265.com/");
                Prolist.Add(p);
            }


            //grid1.ItemsSource = Prolist;
            gridpage.ShowPages(grid1, Helper.ToDataTable(Prolist), 5);

        }

    }

    public class Products
    {
        public int ProID { get; set; }
        public string ProName { get; set; }
        public ProductCategory ProCategory { get; set; }
        public string ProImage { get; set; }
        public Uri ProLink { get; set; }
    }

    public enum ProductCategory
    {
        Travel, Deception, Tools, General
    }

}
﻿using System;
using System.Windows;
using mqtttLib;
using mqtttLib.Helper;
using mqtttLib.Messages.Base;
using ServerWpf.Helper;
using ServerWpf.PagerTest;

namespace ServerWpf
{
    /// <summary>
    /// Main.xaml 的交互逻辑
    /// </summary>
    public partial class Main
    {
        public Main()
        {
            InitializeComponent();
            TxtPort.Text = ConfigureAppConfig.GetConnectionStringsValue("Port");
            TxtIp.Text = ConfigureAppConfig.GetConnectionStringsValue("Ip");
            BtnStart_Click(null, null);
        }
        /// <summary>
        /// 通信服务
        /// </summary>
        private MqttServer _communServer;

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RtMessage.Clear();
                int port;
                if (!int.TryParse(TxtPort.Text, out port))
                {
                    MessageBox.Show(@"输入值不是合法的数字", @"错误");
                    return;
                }
                var dd = new LogInfo("log4net.config", "");
                //通信服务
                _communServer = new MqttServer(port, dd);
                _communServer.LogEvent += _communServer_LogEvent;
                _communServer.ConnectValidatEvent += _communServer_ConnectValidatEvent;
                _communServer.Start();
                //btnStart.Enabled = false;
                //btnStop.Enabled = true;

                BtnStart.IsEnabled = false;
                BtnStop.IsEnabled = true;
                //RtMessage.AddLine("开始监听");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"错误");
                //  ILog log = LogFactory.GetLogger("Main");
                //  log.Debug(ex);

            }
        }

        private bool _communServer_ConnectValidatEvent(string userName, string password)
        {
            return true;
        }

        private void _communServer_LogEvent(string title, MqttMessage data)
        {
            RtMessage.Dispatcher.Invoke(
                       () =>
                       {
                           if (CkbShow.IsChecked == true)
                           {
                               if (CkbHex.IsChecked == true)
                               {

                                   RtMessage.AddLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + title);
                                   RtMessage.AddLine(data.GetDataBytes().GetHexLog());


                               }
                               else
                               {
                                   RtMessage.AddLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + title);
                                   RtMessage.AddLine(data.ToJson());

                               }

                           }

                       }
                );


        }


        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            if (_communServer != null)
            {
                _communServer.Stop();
                _communServer = null;
            }
            BtnStart.IsEnabled = true;
            BtnStop.IsEnabled = false;
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            RtMessage.Clear();
        }
        private void SetConfig_Click(object sender, RoutedEventArgs e)
        {
            SetConfig setConfig = new SetConfig();

            setConfig.UpdateEvent += () =>
            {
                TxtIp.Text = ConfigureAppConfig.GetConnectionStringsValue("Ip");
                TxtPort.Text = ConfigureAppConfig.GetConnectionStringsValue("Port");

                BtnStop_Click(null, null);
            };
            setConfig.Show();
        }


        private void MetroButton_Click_1(object sender, RoutedEventArgs e)
        {
            Window1 Window1 = new Window1();
            Window1.Show();

        }
    }


}

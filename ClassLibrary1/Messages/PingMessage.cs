﻿namespace nMqtt.Messages
{
	public sealed class PingReqMessage : MqttMessage
    {
        public PingReqMessage()
            : base(MessageType.PINGREQ)
        {
        }
    }

    public class PingRespMessage : MqttMessage
    {
        public PingRespMessage()
            : base(MessageType.PINGRESP)
        {
        }
    }
}

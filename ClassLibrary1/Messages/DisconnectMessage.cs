﻿using System.IO;

namespace nMqtt.Messages
{
    public sealed class DisconnectMessage : MqttMessage
    {
        public DisconnectMessage()
            : base(MessageType.DISCONNECT)
        {
        }
    }
}
